import  React from 'react';
import  { AppRegistry,View } from 'react-native';
import Header from './src/components/Header';
import List from './src/components/List';

const App = () => (
    <View style={{ flex: 1 }}>
        <Header headerText={'JinnyJoe'}/>
        <List />
    </View>
);

AppRegistry.registerComponent('jinnyjoe', () => App);
